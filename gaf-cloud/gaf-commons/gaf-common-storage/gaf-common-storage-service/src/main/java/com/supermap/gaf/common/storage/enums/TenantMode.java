package com.supermap.gaf.common.storage.enums;

/**
 * @author heykb
 */
public enum TenantMode {
    SINGLE_NODE_MULTI_BUCKET, SINGLE_NODE_MULTI_PATH, NONE;
}